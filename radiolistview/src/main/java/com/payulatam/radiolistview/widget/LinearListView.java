package com.payulatam.radiolistview.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;


public class LinearListView extends LinearLayout {
  public LinearListView(Context context) {
    super(context);
  }

  public LinearListView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public LinearListView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }
}
