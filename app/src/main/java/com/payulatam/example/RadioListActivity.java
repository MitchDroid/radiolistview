package com.payulatam.example;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.payulatam.example.adapter.RadioListAdapter;
import com.payulatam.example.model.CreditCardType;
import com.payulatam.example.model.CreditCardViewInfo;
import com.payulatam.radiolistview.widget.RadioGroupListView;
import java.util.ArrayList;
import java.util.List;

public class RadioListActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

  private RadioListAdapter adapter;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    adapter = new RadioListAdapter(RadioListActivity.this);
    RadioGroupListView tokensCardListView =
        (RadioGroupListView) findViewById(R.id.radioGroupListView);
    tokensCardListView.setAdapter(adapter);
    tokensCardListView.setCheckedChangeListener(RadioListActivity.this);
    populateData();
    if (savedInstanceState == null) {
      tokensCardListView.selectFirstItem();
    }
  }

  private void populateData() {
    List<CreditCardViewInfo> cards = new ArrayList<>();
    cards.add(new CreditCardViewInfo("****0808", CreditCardType.VISA, true));
    cards.add(new CreditCardViewInfo("****1206", CreditCardType.MASTERCARD, true));
    cards.add(new CreditCardViewInfo("****7912", CreditCardType.AMEX, false));
    cards.add(new CreditCardViewInfo("****5636", CreditCardType.DINERS, true));
    adapter.notifyDataSetChanged(cards);
  }

  @Override protected int getLayoutResource() {
    return R.layout.activity_radio_list;
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_radio_list, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    return id == R.id.action_settings || super.onOptionsItemSelected(item);
  }

  @Override public void onCheckedChanged(RadioGroup group, int checkedId) {
    CreditCardViewInfo creditCardViewInfoSelected = (CreditCardViewInfo) adapter.getItem(checkedId);
    String pan = creditCardViewInfoSelected.getPan();
    String type = creditCardViewInfoSelected.getType().name();
    String message = String.format("Credit Card selected %s %s", pan, type);
    Toast.makeText(RadioListActivity.this, message, Toast.LENGTH_SHORT).show();
  }
}
