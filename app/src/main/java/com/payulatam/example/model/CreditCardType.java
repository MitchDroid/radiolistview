package com.payulatam.example.model;

public enum CreditCardType {

  /** Unknown Card Type */
  UNKNOWN,
  /** Visa type. */
  VISA,
  /** Diners type. */
  DINERS,
  /** Amex type. */
  AMEX,
  /** MasterCard type. */
  MASTERCARD;

  /**
   * Return a {@link CreditCardType} from a given string.
   *
   * @param cardType the string card type.
   * @return the {@link CreditCardType} if the type exist. null otherwise.
   */
  public static CreditCardType fromString(String cardType) {

    if (cardType != null) {

      for (CreditCardType type : CreditCardType.values()) {

        if (type.name().equalsIgnoreCase(cardType)) {
          return type;
        }
      }
    }
    return UNKNOWN;
  }

}
