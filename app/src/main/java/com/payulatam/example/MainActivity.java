package com.payulatam.example;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends BaseActivity implements View.OnClickListener {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    findViewById(R.id.buttonPureListView).setOnClickListener(this);
    findViewById(R.id.buttonRadioGroupList).setOnClickListener(this);
  }

  @Override protected int getLayoutResource() {
    return R.layout.activity_main;
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main_menu, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    return id == R.id.action_settings || super.onOptionsItemSelected(item);
  }

  @Override public void onClick(View v) {

    switch (v.getId()) {
      case R.id.buttonPureListView:
        startActivity(new Intent(MainActivity.this, PureListViewActivity.class));
        break;
      case R.id.buttonRadioGroupList:
        startActivity(new Intent(MainActivity.this, RadioListActivity.class));
        break;
    }
  }
}
